#include <iostream>

// Генератор диагональных матриц
void FillMatrix(float* matrix, int height, int width) {
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if (i == j) {
                matrix[i * width + j] = 1;
            } else {
                matrix[i * width + j] = 0;
            }
        }
    }
}

// Проверка, что матрица диагональная
void CheckMatrix(float *matrix, int height, int width) {
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if (i == j && matrix[i*width + j] == 1)
                continue;
            else if (j != i && matrix[i*width + j] == 0)
                continue;
            else
            {
                std::cout << "fail!" << std::endl;
                return;
            }
        }
    }
    std::cout << "ok" << std::endl;
}

__global__
void MatrixMul2D(float* A, float* B, float* C, int mid_size) {
    // blockDim 16 x 16
    // gridDim   8 x 16
    // threadIdx 0...16 0...16
    // blockIdx  0...8  0...16
    // A 128 x 384
    // B 384 x 256
    // C 128 x 256

    auto i = blockIdx.x * blockDim.x + threadIdx.x;
    auto j = blockIdx.y * blockDim.y + threadIdx.y;

    auto width = blockDim.y * gridDim.y;

    for (int k = 0; k < mid_size; ++k) {
        C[i * width + j] += A[i * mid_size + k] * B[k * width + j];
    }
}

__global__
void MatrixMul(float* A, float* B, float* C, int mid_size) {
    // blockDim  = 256
    // gridDim   = 128
    // threadIdx = 0...256
    // blockIdx  = 0...128
    // A 128 x 384
    // B 384 x 256
    // C 128 x 256
}



int main() {

    float *h_A;
    float *h_B;
    float *h_C;

    cudaSetDevice (3);

    h_A = new float[128 * 384];
    h_B = new float[384 * 256];
    h_C = new float[128 * 256];

    FillMatrix(h_A, 128, 384);
    FillMatrix(h_B, 384, 256);

    float* d_A;
    float* d_B;
    float* d_C;

    cudaMalloc(&d_A, sizeof(float) * 128 * 384);
    cudaMalloc(&d_B, sizeof(float) * 384 * 256);
    cudaMalloc(&d_C, sizeof(float) * 128 * 256);

    cudaMemcpy(d_A, h_A, sizeof(float) * 128 * 384, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, sizeof(float) * 384 * 256, cudaMemcpyHostToDevice);

    cudaEvent_t start;
    cudaEvent_t stop;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);

#if 1
    dim3 num_blocks(8, 16); // 128
    dim3 block_size(16, 16); // 256
    cudaEventRecord(start);
    MatrixMul2D<<<num_blocks, block_size>>>(d_A, d_B, d_C, 384);
#else
    cudaEventRecord(start);
    MatrixMul<<<128, 256>>>(d_A, d_B, d_C, 384);
#endif
    cudaEventRecord(stop);

    cudaMemcpy(h_C, d_C, sizeof(float) * 128 * 256, cudaMemcpyDeviceToHost);
    cudaEventSynchronize(stop);
    CheckMatrix(h_C, 128, 256);

    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << milliseconds << "ms" << std::endl;

    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    delete[] h_A;
    delete[] h_B;
    delete[] h_C;

    return 0;
}
